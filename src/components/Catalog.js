import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './Catalog.module.css';
import { Basket } from './Basket';
import { selectCount, selectCash, basketInner, allItems } from '../store/reducers/reposReducer';

export default function Catalog() {


    const dispatch = useDispatch();
    const [incrementAmount, setIncrementAmount] = useState(0);
    const incrementValue = Number(incrementAmount) || 0;
    const [inputStyles, setStyle] = useState({

    });

    const addItem = (item) => {
        const defaultItem = {

            product: {
                "id": item.id,
                "title": item.title,
                "price": item.price,
                "description": item.description,
                "category": item.category,
                "image": item.image,
                "rating": {
                    "rate": item.rating.rate,
                    "count": item.rating.count
                }
            },
            count: 1


        }
        dispatch({ type: "ADD_COUNT", payload: defaultItem })
    }

    


    const minItem = (id) => {
        dispatch({ type: "MIN_COUNT", payload: id })
    }

    const onChange = (e) => {
        var max = parseInt(e.target.max);
        // console.log(e)

        if (parseInt(e.target.value) > max) {
            e.target.value = max;

            
            setStyle({
                border: "2px solid red"
            })
        }
        setIncrementAmount(Math.abs(e.target.value))

    }
    const deleteItem = (id) => {
        dispatch({ type: "DEL_ITEM", payload: id })
    }

    const byAmount = (item, theAmount) => {

        const defaultItem = {

            product: {
                "id": item.id,
                "title": item.title,
                "price": item.price,
                "description": item.description,
                "category": item.category,
                "image": item.image,
                "rating": {
                    "rate": item.rating.rate,
                    "count": item.rating.count
                }
            },
            count: theAmount
        }
        dispatch({ type: "BY_AMOUNT", payload: defaultItem, amount: theAmount })
    }


    const inner = useSelector(basketInner);
    const all = useSelector(allItems);
    const cash = useSelector(selectCash);
    const count = useSelector(selectCount);


    return (
        <>
            <div className="catalog">

                <div className="wrapper">
                    {all.map(product => {
                        return (
                            <div key={product.id} className="grid">
                                <a href={product.image}><img src={product.image} alt="" /></a>
                                <div className="grid__title">
                                    {product.title}
                                </div>
                                <div className="grid__price">
                                    {product.price}$
                                </div>
                                <div className="grid__input">
                                    <input
                                        className={styles.textbox}
                                        aria-label="Set increment amount"
                                        type="number"
                                        placeholder="Скільки?"
                                        onChange={onChange}
                                        style={inputStyles}
                                        min="1"
                                        max={product.rating.count}
                                    />
                                    <button
                                        className={styles.button}
                                        onClick={() => {
                                            console.log(inputStyles)
                                            byAmount(product, incrementValue)
                                        }}
                                    >
                                        Добавити шт
                                    </button>
                                </div>
                                <div className="grid__button">
                                    {/* <button
                                        className={styles.button}
                                        aria-label="Increment value"
                                        onClick={() => { deleteItem(product.id) }}
                                    >
                                        Del
                                    </button>
                                    <button
                                        className={styles.button}
                                        aria-label="Increment value"
                                        onClick={() => { minItem(product.id) }}
                                    >
                                        Відняти
                                    </button> */}
                                    <button
                                        className={styles.button}
                                        aria-label="Increment value"
                                        onClick={() => { addItem(product) }}
                                    >
                                        Добавити
                                    </button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <Basket cash={cash} arr={inner} count={count} add={addItem} min={minItem} del={deleteItem} />
        </>
    )



}
