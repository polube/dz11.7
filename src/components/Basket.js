/* eslint-disable array-callback-return */
import styles from './Basket.module.css';
import React, { useState } from 'react';


export function Basket(props) {

    const [isBasketOpen, setBasketOpen] = useState(false)

    return (
        <>
            <button className={styles.basket_icon} onClick={() => setBasketOpen(true)}> 
                <i className={styles.fas}>К</i>
                <div className={styles.basket_icon__number}>
                    <span className={styles.cart__bold}>{props.count}</span>
                </div>
            </button>
            <div className={styles.basket} style={isBasketOpen ? {display: 'block'} : {display: "none"}}>
                <div className={styles.basket__inner}>
                    <div className={styles.basket__inner__box}>
                        <div onClick={() => setBasketOpen(false)} className={styles.basket_closer}><span className={styles.closer}>X</span></div>

                        <div className={styles.basket__inner__title}>
                            Корзина
                        </div>
                        <div className={styles.cart}>
                            <form className={styles.form}>
                                <div className={styles.cart__items}>

                                    {props.arr.map(product => {

                                        if (product.count > 0) {

                                            return (
                                                <div className={styles.cart_item} key={product.product.id}>
                                                    <div className={styles.cart_item__main}>
                                                        <div className={styles.cart_item__start}>
                                                            <button className={styles.cart_item__btn} onClick={() => props.del(product.product.id)} type="button">X</button>
                                                        </div>
                                                        <div className={styles.cart_item__img_wrapper}>
                                                            <img className={styles.cart_item__img} src={product.product.image} alt="" />
                                                        </div>
                                                        <div className={styles.cart_item__content}>
                                                            <h3 className={styles.cart_item__title}>{product.product.title}</h3>
                                                        </div>
                                                    </div>
                                                    <div className={styles.cart_item__end}>
                                                        <div className={styles.cart_item__actions}>
                                                            <button className={styles.cart_item__btn} onClick={() => props.min(product.product.id)} type="button">-</button>
                                                            <span className={styles.cart_item__quantity}>{product.count}</span>
                                                            <button className={styles.cart_item__btn} onClick={() => props.add(product.product)} type="button">+</button>
                                                        </div>
                                                        <p className={styles.cart_item__price}>{product.product.price}$</p>
                                                    </div>
                                                </div>
                                            )

                                        }

                                    })}



                                </div>
                                <div className={styles.basket__inner__cash}>
                                    <div className={styles.basket__inner__cash__left}>
                                        <p className={styles.basket__inner__cash__text}>Всього: </p> <span className={styles.cart__bold}><span
                                            className={styles.summ}>{props.cash.toFixed(1)} $</span>
                                        </span>
                                    </div>
                                    <div className={styles.basket__inner__cash__right}>
                                        <button className={styles.buy}>Замовити</button>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </>)
}
