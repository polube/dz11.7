import React from 'react';
import Catalog from './components/Catalog';
import { useDispatch } from "react-redux";
import axios from 'axios';

function App() {
  const dispatch = useDispatch();
  const apiURL = "https://fakestoreapi.com/products";


  const addItem = (item) => {
    dispatch ({ type: "INIT", payload: item })
  }

  const fetchData = async () => {
    const response = await axios.get(apiURL)
    addItem(response.data)
  }
  fetchData();


  return (
    <Catalog />
  );
}

export default App;
