// import products from "../../data/dataset.json";
const ADD_COUNT = "ADD_COUNT";
const MIN_COUNT = "MIN_COUNT";
const DEL_ITEM = "DEL_ITEM";
const BY_AMOUNT = "BY_AMOUNT";
// const INIT = '@@INIT';
const INIT = 'INIT';

const defaultState = {
    items: [],
    cart: []
};

export default function reposReducer(state = defaultState, action) {
    switch (action.type) {

        case INIT:
            return {
                ...state,
                items: action.payload
            };

        case ADD_COUNT:

            let isNew = true;
            if (state.cart.length > 0) {
                const arr = state.cart.map((item) => {
                    if (item.product.id === action.payload.product.id) {
                        isNew = false;
                        return {
                            product: item.product,
                            count: item.count + 1
                        }
                    }
                    return item;
                })
                if (isNew) arr.push(action.payload);
                return {
                    ...state,
                    cart: arr
                }
            } else return {
                ...state,
                cart: [...state.cart, action.payload]
            }


        case MIN_COUNT: {

            if (state.cart.length > 0) {
                let is = false;
                const arr = state.cart.map((item) => {
                    if (item.product.id === action.payload) { //&& item.count > 0
                        if (item.count - 1 <= 0) {
                            
                            is = true;

                        } else if (is === false) return {
                            product: item.product,
                            count: item.count - 1
                        }
                    }
                    return item;
                })

                if (is === true) return {
                    ...state,
                    cart: state.cart.filter(item => item.product.id !== action.payload)
                };

                return {
                    ...state,
                    cart: arr
                }
            }
            return {
                ...state
            }


        }

        case DEL_ITEM: {
            return {
                ...state,
                cart: state.cart.filter(item => item.product.id !== action.payload)
            };
        }

        case BY_AMOUNT: {

            let isNew = true;
            if (state.cart.length > 0) {
                const arr = state.cart.map((item) => {
                    if (item.product.id === action.payload.product.id) {
                        isNew = false;
                        return {
                            product: item.product,
                            count: item.count + action.amount
                        }
                    }
                    return item;
                })
                if (isNew) arr.push(action.payload);
                return {
                    ...state,
                    cart: arr
                }
            } else return {
                ...state,
                cart: [...state.cart, action.payload]
            }
        }

        default: {
            return state;
        }
    }
}

export const selectCash = (state) => {
    let summ = 0;
    for (let i = 0; i < state.repos.cart.length; i++) {
        summ += state.repos.cart[i].product.price
        summ = summ * state.repos.cart[i].count
    }
    return summ
};

export const selectCount = (state) => {
    let summ = 0;
    for (let i = 0; i < state.repos.cart.length; i++) {
        summ += state.repos.cart[i].count
    }
    return summ;
};

export const basketInner = (state => state.repos.cart)
export const allItems = (state => state.repos.items)



