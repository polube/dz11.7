// const item = [];
function setItem(item) {
    return {
        product: {
            "id": item.id,
            "title": item.title,
            "price": item.price,
            "description": item.description,
            "category": item.category,
            "image": item.image,
            "rating": {
                "rate": item.rating.rate,
                "count": item.rating.count
            }
        }
    }
}

export const addItem = (item) => ({
    type: "ADD_COUNT", payload: setItem(item)
});



export const minItem = (id) => ({
    type: "MIN_COUNT", payload: id
})



export const deleteItem = (id) => ({
    type: "DEL_ITEM", payload: id
})


export const byAmount = (item, theAmount) => ({
    type: "BY_AMOUNT", payload: setItem(item), amount: theAmount
})
